<?php

namespace App\Modules\Base\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

use App\Modules\Base\Models\Mensaje;

class MensajeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['Incidencia Nueva'],
            ['Incidencia En Proceso'],
            ['Incidencia Cerrada']
        ];

        DB::beginTransaction();
        try{
            foreach ($data as $mensaje) {
                Mensaje::create([
                    'mensaje'      => $mensaje[0]
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
