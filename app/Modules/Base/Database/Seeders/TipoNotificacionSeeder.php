<?php

namespace App\Modules\Base\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

use App\Modules\Base\Models\TipoNotificacion;

class TipoNotificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['Incidencia Nueva', 'incidencia'],
            ['Incidencia en Proceso', 'incidencia/info/{{$id}}'],
            ['Incidencia Cerrada', 'incidencia']
        ];

        DB::beginTransaction();
        try{
            foreach ($data as $tipo_incidencias) {
                TipoNotificacion::create([
                    'nombre'      => $tipo_incidencias[0],
                    'ruta' => $tipo_incidencias[1]
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
