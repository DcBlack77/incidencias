@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top') 
    @include('base::partials.ubicacion', ['ubicacion' => ['Notificaciones']]) 
@endsection

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Notificaciones</div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table id="tabla3" class="table table-striped table-hover table-bordered tables-text table-condensed" >

                        @foreach($Todasnotificaciones as $notificaciones)
                            <tr>
                            <td class="active">{{ $notificaciones->id}}</td>
                            <td class="active">{{ $notificaciones->usuario_id }}</td>
                            <td class="success">{{ $notificaciones->enviado_id }}</td>
                            <td class="warning">{{ $notificaciones->mensaje_id }}</td>
                            <td class="warning">{{ $notificaciones->operacion_id }}</td>
                            <td class="danger">{{ $notificaciones->visto }}</td>
                             <td class="danger">{{ $notificaciones->tipo_notificacion_id }}</td>
                             <td class="success">{{ $notificaciones->created_at  }}</td>
                             <td class="active">{{ $notificaciones->updated_at }}</td>
                             <td class="active">{{ $notificaciones->deleted_at }}</td>
                            </tr>    
                             @endforeach
                    </table>
              <center>  {{ $Todasnotificaciones->render() }} </center>
                </div>
            </div>
        </div>
    </div>    
@endsection