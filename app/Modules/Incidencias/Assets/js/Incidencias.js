var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"titulo","name":"titulo"},{"data":"descripcion","name":"descripcion"},{"data":"app_usuario_id","name":"app_usuario_id"},{"data":"app_perfil_id","name":"app_perfil_id"},{"data":"estatus_id","name":"estatus_id"},{"data":"correo","name":"correo"},{"data":"nuevo","name":"nuevo"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});