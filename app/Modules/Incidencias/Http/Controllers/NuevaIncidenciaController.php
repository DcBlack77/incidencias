<?php

namespace App\Modules\Incidencias\Http\Controllers;

//Controlador Padre
use App\Modules\Incidencias\Http\Controllers\Controller;
//Modelos
use App\Modules\Incidencias\Http\Requests\NuevaIncidenciaRequest;
use App\Modules\Incidencias\Models\NuevaIncidencia;
use App\Modules\Heroes\Models\IncidenciasImagenes;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Validator;
use Image;

//Request


class NuevaIncidenciaController extends Controller
{
    //protected $titulo = 'Nueva Incidencia';

    public $js = [
        'NuevaIncidencia'
    ];
  

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'ckeditor',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop',

    ];

    public function index()
    {
        return $this->view('incidencias::NuevaIncidencia');
    }
    public function data($request)
    {
        $data = $request->all();
        return $data;
    }

    public function guardar(NuevaIncidenciaRequest $request, $id = 0)
    {

        DB::beginTransaction();
        try {
            $data     = $this->data($request);

            $archivos = json_decode($request->archivos);
           
            if ($id === 0) {
                $NuevaIncidencia = NuevaIncidencia::create($data);
                $id     = $NuevaIncidencia->id;
            } else {
                if (empty($archivos)) {
                    unset($data['published_at']);
                }
                $NuevaIncidencia = NuevaIncidencia::find($id)->update($data);
            }
       // dd($archivos);

            $this->guardarImagenes($archivos, $id);

            //$this->procesar_permisos($request, $id);
        } catch (QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();
        return [
            'id'    => $NuevaIncidencia->id,
            'texto' => $NuevaIncidencia->titulo,
            's'     => 's',
            'msj'   => trans('controller.incluir'),
        ];

    }
    protected function getRuta()
    {
        return date('Y') . '/' . date('m') . '/';
    }
     protected function guardarImagenes($archivos, $id = 0)
    {
        foreach ($archivos as $archivo => $data) {
            if (!preg_match("/^(\d{4})\-(\d{2})\-([0-9a-z\.]+)\.(jpe?g|png)$/i", $archivo)) {
                continue;
            }
            $archivo  = str_replace('-', '/', $archivo);
            $imagen   = Image::make(public_path('archivos/NuevaIncidencia/' . $archivo));
            $imagenes = ImagenesH::firstOrNew(array('archivo' => $archivo)); 

            $imagenes->fill([

                'NuevaIncidencia_id'   => $id,
                'tamano'      => $imagen->width() . 'x' . $imagen->height(),
                'leyenda'     => $data->leyenda,
                'descripcion' => $data->descripcion,

            ]);
            $imagenes->save();
        }
    }

    public function eliminar(Request $request, $id = 0)
    {
        try {
            $rs = NuevaIncidencia::destroy($id);
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }
        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            NuevaIncidencia::withTrashed()->find($id)->restore();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }
        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            NuevaIncidencia::withTrashed()->find($id)->forceDelete();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function eliminarImagen(Request $request, $id = 0)
    {
        $id = str_replace('-', '/', $id);
        try {
            // \File::delete(public_path('img/noticias/' . $id));
            $rs = Imagenes::where('archivo', $id)->delete();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function subir(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'files.*' => [
                'required',
                'mimes:jpeg,jpg,png',
            ],
        ]);

        if ($validator->fails()) {
            return 'Error de Validación';
        }

        $files = $request->file('files');
        $url   = $this->ruta();
        $url   = substr($url, 0, strlen($url) - 6);

        $rutaFecha = $this->getRuta();
        $ruta      = public_path('archivos/incidencias/' . $rutaFecha);

        $respuesta = array(
            'files' => array(),
        );

        foreach ($files as $file) {
            do {
                $nombre_archivo = $this->random_string() . '.' . $file->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));

            $id = str_replace('/', '-', $rutaFecha . $nombre_archivo);

            $respuesta['files'][] = [
                'id'           => $id,
                'name'         => $nombre_archivo,
                'size'         => $file->getSize(),
                'type'         => $file->getMimeType(),
                //'url' => url('imagen/small/' . $rutaFecha . $nombre_archivo),
                'url'          => url('public/archivos/incidencias/' . $rutaFecha . $nombre_archivo),
                'thumbnailUrl' => url('public/archivos/incidencias/' . $rutaFecha . $nombre_archivo),                'deleteType'   => 'DELETE',
                'deleteUrl'    => url($url . '/eliminarimagen/' . $id),
                'data'         => [
                    'cordenadas'  => [],
                    'leyenda'     => '',
                    'descripcion' => '',
                ],
            ];

            $mover = $file->move($ruta, $nombre_archivo);
        }

        return $respuesta;
    }

    protected function random_string($length = 10)
    {
        $key  = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


  

   
    // protected function guardarEtiquetas($request, $id) {
    //     noticias_etiquetas::where('noticias_id', $id)->delete();
    //     foreach ($request['etiquetas_id'] as $etiqueta) {
    //         noticias_etiquetas::create([
    //             'noticias_id' => $id,
    //             'etiquetas_id' => $etiqueta,
    //         ]);
    //     }
    // }

    public function datatable()
    {
        $sql = NuevaIncidencia::select([
            'NuevaIncidencia.titulo', 'NuevaIncidencia.resumen', 'NuevaIncidencia.descripcion', 'NuevaIncidencia.Departamento',
        ]);

        return Datatables::of($sql)->setRowId('id')->make(true);

    }

}


