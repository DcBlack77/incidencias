<?php

namespace App\Modules\Incidencias\Http\Requests;

use App\Http\Requests\Request;

class NuevaIncidenciaRequest extends Request {
    protected $reglasArr = [
        'departameto'		=> ['required'],
        'direccion' 		=> ['required'],
        'numero'			=> ['required'],
        'titulo'			=> ['required'],
        'descripcion'		=> ['required'],
        'published_at' 		=> [''],
    ];
}
