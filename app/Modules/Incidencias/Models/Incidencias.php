<?php

namespace App\Modules\Incidencias\Models;

use App\Modules\Base\Models\Modelo;

use app_usuario;
use app_perfil;
use estatus;

class Incidencias extends Modelo
{
    protected $table = 'incidencias';
    protected $fillable = ["titulo","descripcion","app_usuario_id","app_perfil_id","estatus_id","correo","nuevo"];
    protected $campos = [
    'titulo' => [
        'type' => 'text',
        'label' => 'Titulo',
        'placeholder' => 'Titulo del Incidencias'
    ],
    'descripcion' => [
        'type' => 'textarea',
        'label' => 'Descripcion',
        'placeholder' => 'Descripcion del Incidencias'
    ],
    'app_usuario_id' => [
        'type' => 'select',
        'label' => 'App Usuario',
        'placeholder' => '- Seleccione un App Usuario',
        'url' => 'app_usuario'
    ],
    'app_perfil_id' => [
        'type' => 'select',
        'label' => 'App Perfil',
        'placeholder' => '- Seleccione un App Perfil',
        'url' => 'app_perfil'
    ],
    'estatus_id' => [
        'type' => 'select',
        'label' => 'Estatus',
        'placeholder' => '- Seleccione un Estatus',
        'url' => 'estatus'
    ],
    'correo' => [
        'type' => 'text',
        'label' => 'Correo',
        'placeholder' => 'Correo del Incidencias'
    ],
    'nuevo' => [
        'type' => 'text',
        'label' => 'Nuevo',
        'placeholder' => 'Nuevo'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['app_usuario_id']['options'] = AppUsuario::pluck('personas_id', 'id');
		$this->campos['app_perfil_id']['options'] = AppPerfil::pluck('nombre', 'id');
		$this->campos['estatus_id']['options'] = Estatus::pluck('nombre', 'id');
    }

    public function incidencias/_models/_incidencias()
            {
                return $this->hasMany('App\Modules\Incidencias\Models\Incidencias/Models/Incidencias');
            }
}