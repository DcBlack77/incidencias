<?php

namespace App\Modules\Incidencias\Models;

use App\Modules\Base\Models\Modelo;

class imagenesNueva extends Modelo
{
    protected $table = 'imagenesnuevas';
    protected $fillable = ["nuevaincidencia_id","url"];

   
}