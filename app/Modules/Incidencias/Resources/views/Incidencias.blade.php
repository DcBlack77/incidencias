@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Incidencias']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Incidencias.',
        'columnas' => [
            'Titulo' => '14.285714285714',
		'Descripcion' => '14.285714285714',
		'App Usuario' => '14.285714285714',
		'App Perfil' => '14.285714285714',
		'Estatus' => '14.285714285714',
		'Correo' => '14.285714285714',
		'Nuevo' => '14.285714285714'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Incidencias->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection